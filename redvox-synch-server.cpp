#include <iostream>
#include <sys/time.h>

#include <uWS/uWS.h>

#include "redvox-synch-server.hpp"

int main()
{
    std::cout << getTimestampEpochMicrosecondsUtc() << " Starting redvox-synch-server"  << std::endl;

    uWS::Hub h;

    /* Called every time this server receives a message. */
    h.onMessage([](uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode) {
        const uint64_t receiveTime = getTimestampEpochMicrosecondsUtc();

        if(DEBUG) {
            uint64_t debugTime = getTimestampEpochMicrosecondsUtc();
            for(size_t i = 0; i < length; i++) {
                std::cout << debugTime << " RECV:" << +message[i] << " OPCODE:" << opCode << std::endl;
            }
        }

        if(opCode == uWS::OpCode::BINARY && length == 1 && message[0] <= 1) {
            char m[RESPONSE_SIZE];        

            m[0] = message[0];
            m[1] = (receiveTime >> 56) & 0xFF;
            m[2] = (receiveTime >> 48) & 0xFF;
            m[3] = (receiveTime >> 40) & 0xFF;
            m[4] = (receiveTime >> 32) & 0xFF;
            m[5] = (receiveTime >> 24) & 0xFF;
            m[6] = (receiveTime >> 16) & 0xFF;
            m[7] = (receiveTime >> 8)  & 0xFF;
            m[8] = receiveTime         & 0xFF;

            const uint64_t sendTime = getTimestampEpochMicrosecondsUtc();
            m[9]  = (sendTime >> 56) & 0xFF;
            m[10] = (sendTime >> 48) & 0xFF;
            m[11] = (sendTime >> 40) & 0xFF;
            m[12] = (sendTime >> 32) & 0xFF;
            m[13] = (sendTime >> 24) & 0xFF;
            m[14] = (sendTime >> 16) & 0xFF;
            m[15] = (sendTime >> 8)  & 0xFF;
            m[16] = sendTime         & 0xFF;

            if(DEBUG) {
                uint64_t debugTime = getTimestampEpochMicrosecondsUtc();
                std::cout << debugTime << " SEND:" << +m[0] << "," << receiveTime << "," << sendTime << std::endl;
                for(int i = 0; i < RESPONSE_SIZE; i++) {
                    std::cout << debugTime << " SEND: [" << i << "] = " << +m[i] << std::endl;
                }
            }
         
            ws->send(m, RESPONSE_SIZE, uWS::OpCode::BINARY);
        }
        else {
            std::cout << getTimestampEpochMicrosecondsUtc() << " Recv bad request" << std::endl;
            ws->close();
        }
    });

    if (h.listen(SERVER_PORT)) {
        std::cout << getTimestampEpochMicrosecondsUtc() << " Listening on port " << SERVER_PORT << std::endl;
        h.run();
    }
    else {
        std::cout << getTimestampEpochMicrosecondsUtc() << " Error listening on port " << SERVER_PORT << std::endl;
    }
}

/* Returns the current time since the epoch in microseconds UTC. */
inline uint64_t getTimestampEpochMicrosecondsUtc() {
    struct timeval tv{};
    gettimeofday(&tv, nullptr);
    return (((uint64_t)tv.tv_sec) * US_IN_S) + ((uint64_t)tv.tv_usec);
}
