#!/bin/bash

sudo yum install openssl-devel zlib-devel
wget https://github.com/uNetworking/uWebSockets/archive/v0.14.4.tar.gz && 
tar xf v0.14.4.tar.gz && 
rm v0.14.4.tar.gz && 
cd uWebSockets-0.14.4 && 
make && 
sudo make install
cd ..
rm -rf uWebSockets-0.14.4
