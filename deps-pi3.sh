#!/bin/bash

sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install libssl-dev
wget https://github.com/uNetworking/uWebSockets/archive/v0.14.4.tar.gz && 
tar xf v0.14.4.tar.gz && 
rm v0.14.4.tar.gz && 
cd uWebSockets-0.14.4 && 
make && 
sudo make install
cd ..
rm -rf uWebSockets-0.14.4
