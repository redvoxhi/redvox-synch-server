#ifndef REDVOX_SYNCH_SERVER_HPP
#define REDVOX_SYNCH_SERVER_HPP

#include <cstdbool>
#include <cstdint>

#define US_IN_S 1000000
#define SERVER_PORT 9876
#define RESPONSE_SIZE 17 // 1 sequence number byte, two uint64_t timestamps

#define DEBUG true

uint64_t getTimestampEpochMicrosecondsUtc();
#endif /* REDVOX_SYNCH_SERVER_HPP */
