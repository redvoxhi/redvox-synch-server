#!/bin/bash

echo Obtaining redvox-synch-server from bitbucket...
git clone https://achriste@bitbucket.org/redvoxhi/redvox-synch-server.git

echo Updating and obtaining dependencies...
cd redvox-synch-server
sh deps-pi3.sh

echo Building redvox-synch-server...
sh build.sh

echo Installing redvox-synch-server...
sudo cp build/redvox-synch-server /usr/local/bin/.

echo Installing service...
sudo cp redvox-synch-server.service /lib/systemd/system/.
sudo systemctl enable redvox-synch-server.service
sudo systemctl start redvox-synch-server.service

echo Done!

