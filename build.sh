#!/bin/bash

echo -n "Building redvox-synch-server... "
ERRORS="-Wall -Werror"
INCLUDES="-I/usr/include/uWS"
LINKING="-luWS -lssl -lz"
SOURCES="redvox-synch-server.cpp"
g++ ${ERRORS} -O3 -std=c++11 ${INCLUDES} ${LINKING} ${SOURCES} -o redvox-synch-server &&
mkdir -p build &&
mv redvox-synch-server build/.
echo "Done."
