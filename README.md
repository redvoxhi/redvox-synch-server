### Example javascript client

https://redvoxhi.bitbucket.io/redvox-synch-server/

### redvox-synch-server algorithm

In the following algorithm, all _A_ values are produced on the server and the unit for _A_ values is microseconds since the epoch UTC.
All _B_ values are produced on the client and the unit for _B_ values is also microseconds since the epoch UTC.

This algorithm expects that the client will build up a successive message exchange consisting of the elements _B0_, _B1_, _B2_, _B3_, _A1_, _A2_, _A3_.

1. Client connects to redvox-synch-server over WebSocket connection
2. Client records and stores _B0_ immediately before sending message
3. Client sends binary message to server where the payload is a single byte 0x00
4. Server responds asynchronously, goto 5
5. onMessage fires for client
    1. Client records _Btmp_
    2. Client decodes payload (see protocol below)
    3. If sequence number is 0, then
        2. Client extracts and stores _A1_ from  _server receive timestamp_
        1. Client stores _B1_ = _Btmp_
        3. Client records and stores _B2_ immediately before sending message
        4. Client sends binary message to server where payload is a single byte 0x01
        5. Server responds asynchronously, goto 5
    4. If sequence number is 1, then
        1. Client extracts and stores _A2_ from _server receive timestamp_
        2. Client extracts and stores _A3_ from _server send timestamp_
        3. Client stores _B3_ = _Btmp_
        4. Client can close the connection _or_ reuse the same connection to perform another message exchange starting from 2
        
Once each message exchange completes, the exchange can be stored in the following order: A1, A2, A3, B1, B2, B3. Please note that B0 is not used.


### redvox-synch-server protocol

Connections to the redvox-synch-server are made using a binary protocol over WebSockets.

Once a client is connected to the redvox-synch-server, the client can send

* A single unsigned byte representing the sequence number
* This value must be one of **0** or **1**

The server will respond with a 17 byte binary payload where

* The first byte is the client's sequence number
* The next 8 bytes are the server's receive timestamp in microseconds since the epoch UTC
* The final 8 bytes are the server's send timestamp in microseconds since the epoch UTC

The layout of the server payload in table form is as follows:

| Field | Byte Offset | Size |
|-------|-------------|------|
| Sequence Number | 0 | 1 |
| Server Receive Timestamp Epoch Microseconds UTC | 1 | 8 |
| Server Send Timestamp Epoch Microseconds UTC | 9 | 8 |


